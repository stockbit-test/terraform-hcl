# VPC Part
## VPC Creation
resource "aws_vpc" "stockbit-vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  instance_tenancy = "default"

  tags = {
    "Name" = "stockbit-vpc"
    "project" = "stockbit"
  }   
}

# Public Subnet Part
## Public Subnet Creation
resource "aws_subnet" "stockbit-subnet-public-a" {
  vpc_id = aws_vpc.stockbit-vpc.id
  cidr_block = "10.0.0.0/24"
  map_public_ip_on_launch = true
  availability_zone = "${var.AWS_REGION}a"

  tags = {
    "Name" = "stockbit-subnet-public-a"
    "project" = "stockbit"
  }
}

## Internet Gateway Creation
resource "aws_internet_gateway" "stockbit-igw" {
  vpc_id = aws_vpc.stockbit-vpc.id

  tags = {
    "Name" = "stockbit-igw"
    "project" = "stockbit"
  }
}

## Route Tabel public subnet
resource "aws_route_table" "stockbit-crt-public" {
  vpc_id = aws_vpc.stockbit-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.stockbit-igw.id
  }

  tags = {
    "Name" = "stockbit-crt-public"
    "project" = "stockbit"
  }
}

## Association between Public Subnet & Public Custom Route Table
resource "aws_route_table_association" "stockbit-crt-association-public-a" {
  subnet_id = aws_subnet.stockbit-subnet-public-a.id
  route_table_id = aws_route_table.stockbit-crt-public.id
}

# Private Subnet Part
## Private Subnet Creation
resource "aws_subnet" "stockbit-subnet-private-a" {
  vpc_id = aws_vpc.stockbit-vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "${var.AWS_REGION}a"

  tags = {
    Name = "stockbit-subnet-private-a"
    project = "stockbit"
  }
}

## EIP Creation for NAT Gateway
resource "aws_eip" "stockbit-nat-eip-a" {
  vpc = true
  depends_on = [aws_internet_gateway.stockbit-igw]
  
  tags = {
    "Name" = "stockbit-nat-eip-a"
    "project" = "stockbit"
  }
}

## NAT Gateway Creation
resource "aws_nat_gateway" "stockbit-nat-gw-a" {
  allocation_id = aws_eip.stockbit-nat-eip-a.id
  subnet_id = aws_subnet.stockbit-subnet-public-a.id

  tags = {
    "Name" = "stockbit-nat-gw-a"
    "project" = "stockbit"
  }
}

## Route Tabel private subnet
resource "aws_route_table" "stockbit-crt-private" {
  vpc_id = aws_vpc.stockbit-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.stockbit-nat-gw-a.id
  }

  tags = {
    "Name" = "stockbit-crt-private"
    "project" = "stockbit"
  }
}

## Association between Private Subnet & Private Custom Route Table Nat-gw-a
resource "aws_route_table_association" "stockbit-crt-association-private-a" {
  subnet_id = aws_subnet.stockbit-subnet-private-a.id
  route_table_id = aws_route_table.stockbit-crt-private.id
}

# EC2 Auto Scaling Group
## Launch Template creation
resource "aws_launch_template" "stockbit-launch-template" {
  name = "stockbit-launch-template"
  description = "stockbit template for IAC test"
  image_id = "ami-073998ba87e205747"
  instance_type = "t2.medium"

  tag_specifications {
    resource_type = "instance"

    tags = {
    "Name" = "stockbit-instance-group"
    "project" = "stockbit"
    }
  }

  tag_specifications {
    resource_type = "volume"

    tags = {
    "Name" = "stockbit-instance-group"
    "project" = "stockbit"
    }
  }
  
  tag_specifications {
    resource_type = "network-interface"

    tags = {
    "Name" = "stockbit-instance-group"
    "project" = "stockbit"
    }
  }

  tags = {
    "Name" = "stockbit-launch-template"
    "project" = "stockbit"
  }
}

## Auto Scaling group Creation
resource "aws_autoscaling_group" "stockbit-autoscaling-group" {
  name                = "stockbit-autoscaling-group"
  vpc_zone_identifier = [ "${aws_subnet.stockbit-subnet-private-a.id}" ]
  desired_capacity    = 2
  max_size            = 5
  min_size            = 2
  enabled_metrics     = [ "GroupTotalInstances" ]

  launch_template {
    id      = aws_launch_template.stockbit-launch-template.id
    version = "$Latest"
  }

  tags = concat(
    [
      {
        "key"                 = "Name"
        "value"               = "stockbit-autoscaling-group"
        "propagate_at_launch" = true
      },
      {
        "key"                 = "project"
        "value"               = "stockbit"
        "propagate_at_launch" = true
      },
    ]
  )
}

## Auto Scaling Policy
resource "aws_autoscaling_policy" "stockbit-autoscaling-policy" {
  name                    = "stockbit-autoscaling-policy"
  policy_type             = "TargetTrackingScaling"
  autoscaling_group_name  = aws_autoscaling_group.stockbit-autoscaling-group.name

  target_tracking_configuration {
    target_value = 45

    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
  }
}