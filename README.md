# Terraform HCL

## Description

This repository is a statement submission for completion of Terraform HCL related Test. where the tasks that must be done is :
```
Tolong tuliskan Terraform HCL config untuk menghasilkan AWS infra
``` 

## Overview Results

From the given task, I provide two solution options, where the first solution is Terraform config which does not use modules (only uses resources) and the other one uses terraform modules.
The results are as follows :

### With Terraform Modules

To-Do list :  
- [x] `1 VPC`, `1 subnet public`, & `1 subnet private yg terhubung dengan 1 NAT Gateway`  
  Statement Results : https://gitlab.com/stockbit-test/terraform-hcl/-/blob/main/with-tf-modules/main.tf#L2-19
- [x] `1 autoscaling group dengan config :`
  ```
  minimum 2 instance EC2 T2.medium dan max 5 instance, dimana scaling policy adalah CPU >= 45%. Instance harus ditempatkan di 1 subnet Private yang dibuat di poin 3 diatas.
  ```
  Statement Results : https://gitlab.com/stockbit-test/terraform-hcl/-/blob/main/with-tf-modules/main.tf#L22-110

### Without Terraform Modules

To-Do list :  
- [x] `1 VPC`  
  Statement Results : https://gitlab.com/stockbit-test/terraform-hcl/-/blob/main/without-tf-modules/main.tf#L1-13
- [x] `1 subnet public`  
  Statement Results : https://gitlab.com/stockbit-test/terraform-hcl/-/blob/main/without-tf-modules/main.tf#L15-58
- [x] `1 subnet private yg terhubung dengan 1 NAT Gateway`  
  Statement Results : https://gitlab.com/stockbit-test/terraform-hcl/-/blob/main/without-tf-modules/main.tf#L60-114
- [x] `1 autoscaling group dengan config :`  
  ```
  minimum 2 instance EC2 T2.medium dan max 5 instance, dimana scaling policy adalah CPU >= 45%. Instance harus ditempatkan di 1 subnet Private yang dibuat di poin 3 diatas.
  ```  
  Statement Results : https://gitlab.com/stockbit-test/terraform-hcl/-/blob/main/without-tf-modules/main.tf#L116-200

### AWS Infrastructures Design Results

From the HCL config terraform that has been written above, it produces an AWS infrastructure with the following design :  

![1 AZ](img/1-AZ.drawio.svg)

But in order to further improve High Availability, personally for the real case I would suggest to be able to use 3 availability zones and produce a design infrastructure like the following :  

![3 AZ](img/3-AZ.drawio.svg)

## How it Works

### Guides

To be able to deploy the Terraform HCL config script, you only need to do the following things :
- Open the directory Terraform HCL Config script. e.g :
  ```
  cd with-tf-modules
  ```
- Do the terraform init
  ```
  terraform init
  ```
- Do the terraform plan
  ```
  terraform plan
  ```
- Do the terraform apply
  ```
  terraform apply -auto-approve
  ```

### Screenshots Result

Here the screenshots result :  
- 1 VPC

![ss1 Result](img/ss1.png)

- 1 Subnet Public

![ss2 Result](img/ss2.png)

- 1 Subnet Private connected with 1 NAT Gateway

![ss3 Result](img/ss3.png)

- ASG : Minimum 2 Instance & Maximum 5 Instance with EC2 t2.medium

![ss4 Result](img/ss4.png)

- ASG : Scaling Policy is CPU >= 45%

![ss5 Result](img/ss5.png)

- ASG : Deployed in Private Subnet

![ss6 Result](img/ss6.png)