# Provisioning VPC Subnet IG & NAT
module "vpc" {
  source    = "terraform-aws-modules/vpc/aws"
  version   = "3.7.0"

  name = "stockbit-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["ap-southeast-1a"]
  public_subnets  = ["10.0.0.0/24"]
  private_subnets = ["10.0.1.0/24"]

  enable_nat_gateway = true

  tags = {
    Project = "stockbit"
    Environment = "production"
  }
}

# Provisioning Auto Scaling Group
module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "4.6.0"

  # Autoscaling group
  name = "stockbit-asg"

  min_size                  = 2
  max_size                  = 5
  desired_capacity          = 2
  health_check_type         = "EC2"
  vpc_zone_identifier       = module.vpc.private_subnets
  enabled_metrics           = [ "GroupTotalInstances" ]

  instance_refresh = {
    strategy = "Rolling"
    preferences = {
      min_healthy_percentage = 50
    }
    triggers = ["tag"]
  }

  # Launch template
  lt_name                = "stockbit-lt"
  description            = "stockbit template for IAC test"
  update_default_version = true

  use_lt    = true
  create_lt = true

  image_id          = "ami-073998ba87e205747"
  instance_type     = "t2.medium"
  enable_monitoring = true

  tag_specifications = [
    {
      resource_type = "instance"
      tags          = { Project = "stockbit" }
    },
    {
      resource_type = "instance"
      tags          = { Environment = "Production" }
    },
    {
      resource_type = "volume"
      tags          = { Project = "stockbit" }
    },
    {
      resource_type = "volume"
      tags          = { Environment = "Production" }
    },
    {
      resource_type = "network-interface"
      tags          = { Project = "stockbit" }
    },
    {
      resource_type = "network-interface"
      tags          = { Environment = "Production" }
    }
  ]

  tags = [
    {
      key                 = "Environment"
      value               = "production"
      propagate_at_launch = true
    },
    {
      key                 = "Project"
      value               = "stockbit"
      propagate_at_launch = true
    },
  ]
}

## Auto Scaling Policy
resource "aws_autoscaling_policy" "stockbit-asg-policy" {
  name                    = "stockbit-asg-policy"
  policy_type             = "TargetTrackingScaling"
  autoscaling_group_name  = module.asg.autoscaling_group_name

  target_tracking_configuration {
    target_value = 45

    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
  }
}